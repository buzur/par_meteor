Router.configure({
  layoutTemplate: 'layout',
  loadingTemplate: 'loading',
  notFoundTemplate: 'notFound'
});

 	Router.route('/', {name:'index'});
 	// все ообъявления
 	Router.route('/adverts', {
 		name:'advertsList',
 		waitOn: function () {
 		 	return Meteor.subscribe('adverts');
 		} 
 	});
	
	// поиск объявлений	
	Router.route('/search', {
		name:'search'
		// waitOn: function () {
 	// 	 	return Meteor.subscribe('AdvertsSearching');
 	// 	}
	});

	// личный кабинет
	Router.route('/lk', function () {
		this.render('top', {to: 'atop'});
		this.render('lk');
	});

	// объявления, созданные user
	Router.route('/lk/myadverts', function () {
		this.render('top', {to: 'atop'});
		this.render('useradvertsList');
	}, {
		name:'useradvertsList',
		waitOn: function () {
			return Meteor.subscribe('user_adverts');
		}
	});

	// объявлениe
	Router.route('/adverts/:_id', {
		name:'advertItem',
		waitOn: function () {
			return Meteor.subscribe('advert_item',this.params._id)
		},
		data: function() {return Adverts.findOne(this.params._id); }
	});
	//редактирование объявления
	Router.route('/adverts/:_id/edit', {
		name:'advertEdit',
		waitOn: function () {
			return Meteor.subscribe('advert_item',this.params._id)
		},
		data: function() {return Adverts.findOne(this.params._id); }
	});

	Router.route('/addadverts', {name:'advertAdd'});
	// добавление объявления, используя autoform
	Router.route('/addadvertsSchema', {name:'advertAddSchema'});

	Router.route('/favourite', {
		name:'favourite',
		waitOn: function () {
			return Meteor.subscribe('fav');
		},
	});

		var requireLogin = function () {
			if (! Meteor.user()) {
				if (Meteor.loggingIn()) {
			      this.render(this.loadingTemplate);
			    } else {
			      this.render('accessDenied');
			    }
			} else {
				this.next();
			}
		}

	Router.onBeforeAction(requireLogin, {only: 'advertAdd'});
