UsersFavAdd = new Mongo.Collection('UsersFavAdd');

UsersFavAdd.allow ({
	insert: function (userId,doc) {
		return !! userId;
	},
	update: function (userId,doc) {
		return !! userId;
	}
});