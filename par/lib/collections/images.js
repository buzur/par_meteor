Images = new FS.Collection("images", {
  stores: [new FS.Store.GridFS("images")]
});

Images.allow({
  insert:function(){
    return true;
  },
  update:function(){
   return false;
  },
  remove:function(){
    return false;
  },
  download:function(){
    return true;
  }
});
