Adverts = new Meteor.Collection('adverts');

// Adverts.attachSchema (new SimpleSchema({
// 	title: {
// 		type: String,
// 		label: "Название",
// 		max:100,

// 	},
// 	description: {
// 		type: String,
// 		label:"Описание",
// 		max: 200
// 	},
// 	price : {
// 		type: Number,
// 		label:"Стоимость",
// 		min: 0
// 	},
// 	category: {
// 		type: String,
// 		label: "Категория",
// 		max:50,
// 		allowedValues: ['Автомобиль', 'Здание', 'Биллборд'],
// 		defaultValue: 'Автомобиль',
// 		optional: false,
// 	},
// 	position : {
// 		type: String,
// 		label:"Город",
// 		min: 0
// 	},
// 	// start: {
// 	//     type: Date,
// 	//     autoform: {
// 	//       afFieldInput: {
// 	//         type: "bootstrap-datetimepicker"
// 	//       }
// 	//     }
//  //  }
// }));

FavouriteAdverts = new Meteor.Collection('favouriteAdverts')

Adverts.allow ({
	insert: ownerDocument,
	update: ownerDocument,
	remove: ownerDocument
});
