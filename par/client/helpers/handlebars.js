UI.registerHelper('dateconvert',function (date) {
	var converted = moment(date).format("Do MMM YY"); 
	return converted;
});
UI.registerHelper('checked',function () {
	var documentId = this._id;
	var user = Meteor.userId();
	var UsersFavAddItem = UsersFavAdd.findOne({advId:documentId, userId:user});
    var isCompleted = UsersFavAddItem.completed;
    if(isCompleted){
        return "checked";
    } else {
        return "";
    }
});


UI.registerHelper('advertOwner', function (){
    return this.userId == Meteor.userId();
})


UI.registerHelper('buttonArchive', function (){
    // var inArchive = Adverts.findOne(this._id).archived;
    if (this.archived == true) {
        return "Опубликовать"
    } else {
        return "Снять объявление"
    }
})
UI.registerHelper('imageArchive', function (){
    // var inArchive = Adverts.findOne(this._id).archived;
    if (this.archived == true) {
        return "_on"
    } else {
        return ""
    }
})


