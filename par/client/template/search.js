Template.search.rendered = function() {
  var today = new Date();

  $('#datepicker1').datetimepicker({
    format: 'LL',
    minDate: today,
  });

  $('#datepicker2').datetimepicker({
    // useCurrent: false,
    format: 'LL',
    minDate: today,
  });

  $("#datepicker1").on("dp.change", function(e) {
    $('#datepicker2').data("DateTimePicker").minDate(e.date);
  });
  $("#datepicker2").on("dp.change", function(e) {
    $('#datepicker1').data("DateTimePicker").maxDate(e.date);
  });

  $('.selectpicker').selectpicker(); 
};

// Template.search.helpers({
// // 'filters': function() {
// //   return [
// //     {
// //       label: 'auto',
// //       field: 'category',
// //       value: 'auto',
// //       operator: '$eq',
// //       sort: 'desc'
// //     }
// //   ]
// // }
//   'adverts': function () {
//     return Adverts.find();
//   },
// });

// AdvertsFilter = new FilterCollections(Adverts, {
//   name: 'advertsSearching',
//   template: 'search',
//   filters: {
//     "category": {
//       title:'Category',
//       operator: ['$regex', 'i'],
//       condition:'$and',
//       sort: 'desc'
//     }
//   },
// })
