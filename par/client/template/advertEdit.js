Template.advertEdit.rendered =function(){
	var value = Adverts.findOne().category;

	var today = new Date();

	var stardate = Adverts.findOne().start;

	var stopdate = Adverts.findOne().stop;

  	var today = new Date();

	$('#datepicker1').datetimepicker({
		defaultDate: stardate,
		format: 'LL'
	});

	$('#datepicker2').datetimepicker({
	    defaultDate: stopdate,
	    format: 'LL'
	});

	$("#datepicker1").on("dp.change", function (e) {
		$('#datepicker2').data("DateTimePicker").minDate(e.date);
	});
	$("#datepicker2").on("dp.change", function (e) {
		$('#datepicker1').data("DateTimePicker").maxDate(e.date);
	});

	$('.selectpicker').val(value);
	$('.selectpicker').selectpicker('render');
};


Template.advertEdit.events({
	'submit form': function (e,template) {
    e.preventDefault();
    // ПОКА ЧТО НЕ ОБНОВЛЯЕМ КАРТИНКУ
    // var fsFile = new FS.File(template.find('input[type=file]').files[0]);
    // fsFile.owner = Meteor.userId();
    // var picId = Images.insert(fsFile, function (err) {
    //   if (err) throw err;
    // });
    // var TId = picId._id;

    var timeStart = $('#datepicker1').data("DateTimePicker").date().valueOf();
    var timeStop = $('#datepicker2').data("DateTimePicker").date().valueOf();
    var type = $('.selectpicker').val();
	var advertId = this._id;

    var advert = {
      title: $(e.target).find('[id=advertTitle]').val(),
      description: $(e.target).find('[id=advertDescription]').val(),
      position: $(e.target).find('[id=advertPosition]').val(),
      price: $(e.target).find('[id=advertPrice]').val() ,
      category: type,
      start : timeStart,
      stop : timeStop
    };

    Meteor.call('changeAdvert', advert, advertId, function (error, result) {
      if (error)
        return alert(error.reason);
      Router.go('useradvertsList');
    });
  },

  //Подумать как наиболее правильно удалить из Фэйворит, оставить и потом обработать отсутствие, или же просто удалить?
  //Также необходимо удалить картинку объявления
	'click .btn-danger': function (e) {
		e.preventDefault();
		var advertId = this._id;
		Meteor.call('archiveAdvert',advertId, function (error, result) {
	    if (error)
	        return alert(error.reason);
	    Router.go('useradvertsList');
		})
	}
});
  
