Template.advertAddSchema.helpers ({
  categories: function () {
    return [
      {label:"Автомобиль",value:"Автомобиль"},
      {label:"Здание",value:"Здание"},
      {label:"Биллборд",value:"Биллборд"}
    ];
  }
})

Template.advertAddSchema.events({
  'submit form': function (e,template) {
    e.preventDefault();

    var fsFile = new FS.File(template.find('input[type=file]').files[0]);
    fsFile.owner = Meteor.userId();
    var picId = Images.insert(fsFile, function (err) {
      if (err) throw err;
    });
    var TId = picId._id;

    var timeStart = $('#datepicker1').data("DateTimePicker").date().valueOf();
    var timeStop = $('#datepicker2').data("DateTimePicker").date().valueOf();
    var user = Meteor.user();
    var type = $('.selectpicker').val();

    var advert = {
      title: $(e.target).find('[id=advertTitle]').val(),
      description: $(e.target).find('[id=advertDescription]').val(),
      position: $(e.target).find('[id=advertPosition]').val(),
      price: $(e.target).find('[id=advertPrice]').val() ,
      photoUrl:TId,
      category: type,
      start : timeStart,
      stop : timeStop
    };

    Meteor.call('addAdvert', advert, function (error, result) {
      if (error)
        return alert(error.reason);
      Router.go('advertItem', {_id: result._id});
    });
  }
});