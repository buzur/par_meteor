Template.advertItem.helpers({
	'photoIdUrl': function() {
		var image = Images.findOne({_id:this.photoUrl});
		var im = String(image.url());
		if (im!="null") {
			return image.url()
		} else {
			return "../images/helper/no-image.jpg"
		}
		
	}
	
});

Template.advertItem.events({
	'change [type=checkbox]': function(){
    	var documentId = this._id;
    	Meteor.call('change_fav', documentId, function(error, result) {
		    // отобразить ошибку пользователю и прерваться
		    if (error)
		        return alert(error.reason);
		})
    }	
})