Meteor.publishComposite ('advert_item', function(id) {
	return {
	find: function() {
		return Adverts.find(id);
	},
	children : [
		{
		find: function (adv) {
				return Images.find({ _id:adv.photoUrl });
			}
		},
		{
		find: function (adv) {
				return UsersFavAdd.find({ userId: adv.userId, advId: adv._id });
			}
		}
	]
	}
}	
)


Meteor.publishComposite('adverts', {
	find: function () {
		return Adverts.find();		
	},
	children : [
		{
		find: function (adv) {
				return UsersFavAdd.find({ userId: adv.userId });
			}
		},
		{
		find: function (adv) {
				return Images.find({ _id: adv.photoUrl });
			}
		}
	]
}
)


Meteor.publishComposite('user_adverts', {
	find: function () {
		return Adverts.find({userId: this.userId});		
	},
	children : [
		{
		find: function (adv) {
				return Images.find({ _id: adv.photoUrl });
			}
		}
	]
}
)


Meteor.publishComposite('fav',  {
	find: function () {
		return UsersFavAdd.find({userId:this.userId, completed:true});
	},
	children : [
		{
			collectionName: "favouriteAdverts",
			find: function (adv) {
				return Adverts.find({ _id: adv.advId });
		},
		children: [
			{
				// collectionName: "favouriteImages",
				find: function(ad){
					return Images.find({ _id: ad.photoUrl });
				}
			}
		]
		}
	] 	
	}	
)

