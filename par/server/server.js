Meteor.methods({
	addAdvert: function(advertAt) {
		check(Meteor.userId(), String);
	    check(advertAt, {
	    title: String,
			category: String,
			description: String,
			photoUrl: String,
			start: Number,
			stop: Number,
			position: String,
			price: String
	    });
		var user = Meteor.user();
		var advert = _.extend(advertAt,{
			userId: user._id,
			createdAt: new Date(),
      archived: false,
      approved: false
		});
		var advertId = Adverts.insert(advert);
		return {
			_id: advertId
		};
	},
  //архивирование объявления
  archiveAdvert: function(advertId) {
    check(Meteor.userId(), String);
    var advertUser = Adverts.findOne(advertId).userId;
    if (Meteor.userId() != advertUser){
      return error
    } else { 
      var inArchive = Adverts.findOne(advertId).archived
      if (inArchive == false){
        Adverts.update({_id:advertId}, {
          $set:{archived: true}
        })
      } else { 
        Adverts.update({_id:advertId}, {
          $set:{archived: false}
        })
      }
    } 
  },
// редактирование объявления
  changeAdvert: function(advertAt,advertId) {
    check(Meteor.userId(), String);

    var advertUser = Adverts.findOne(advertId).userId;

    if (Meteor.userId() != advertUser) {
      return error
    } else {
      check(advertAt, {
        title: String,
        category: String,
        description: String,
        start: Number,
        stop: Number,
        position: String,
        price: String
      });

      Adverts.update({_id:advertId}, {
        $set:{
          title: advertAt.title,
          category: advertAt.category,
          description: advertAt.description,
          start: advertAt.start,
          stop: advertAt.stop,
          position: advertAt.position,
          price: advertAt.price,
        }
      })
    }
  },
//  Удаление модератором мб , пока что архивная функция
  deleteAdvert: function(advertId) {
    var picId = Adverts.findOne(advertId).photoUrl;

    Adverts.remove({_id:advertId});
    Images.remove({_id:picId})
  },
// Добавление объявления в избранное
	change_fav:function (documentId) {
		check(Meteor.userId(), String);
		check(documentId, String);
		
		var user = Meteor.userId();
		
    if (UsersFavAdd.find({advId:documentId,userId:user}).count()===0) {
    	UsersFavAdd.insert({advId:documentId,userId:user,completed: true })
    } else {
    	var UsersFavAddItem = UsersFavAdd.findOne({advId:documentId,userId:user});
    	var isCompleted = UsersFavAddItem.completed ;
	    if (!isCompleted) {
	    	UsersFavAdd.update({_id:UsersFavAddItem._id},{$set: { completed: true }})
	    } else {
	    	UsersFavAdd.update({_id:UsersFavAddItem._id},{$set: { completed: false }})
	    }
    }
	}
	
});

